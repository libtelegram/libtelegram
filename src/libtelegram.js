const Telegram = require('@libtelegram/telegram');

const { types } = require('./utils');
const { slice } = require('../shared/utils');

const Scene = require('./scene/');

const Route = require('./scene/route');

class LibTelegram extends Telegram {
  constructor (token, opts) {
    super(token);

    this.init(opts);
  }

  init (settings) {
    this.settings = settings || {};
    this.users = [];

    this.mountpath = '/';
  }

  start (fn) {
    this.startPolling();
    
    if (typeof fn === 'function') {
      fn();
    }

    return this;
  }

  stop (fn) {
    this.stopPolling();
    fn();
    return this;
  }

  enter (usr) {
    return this._scene.executeHandlers('enterHandlers', usr);
  }

  leave (usr) {
    return this._scene.executeHandlers('leaveHandlers', usr);
  }

  lazyscene () {
    if (!this._scene) {
      this._scene = new Scene();
      this._scene.path = '/';
    }
  }

  use (fn) {
    let offset = 0,
        path;

    // default path to '/'
    // disambiguate app.use([fn])
    if (typeof fn !== 'function') {
      let arg = fn;
  
      while (Array.isArray(arg) && arg.length !== 0) {
        arg = arg[0];
      }
  
      // first arg is the path
      if (typeof arg !== 'function') {
        offset = 1;
        path = fn;
      }
    }
  
    const fns = slice.call(arguments, offset).flat();
  
    if (fns.length === 0) {
      throw new Error('bot.use() requires a middleware function')
    }
  
    // setup scene
    this.lazyscene();
    const scene = this._scene,
          self = this;
  
    fns.forEach(function (fn) {
      // non-libtelegram app
      if (!fn || !fn.handle) {
        return scene.use(path || [], fn);
      }
  
      //fn.path = path;
      //fn.mountpath = path;
      fn.parent = self;

      scene.use(path || '/', fn);
    }, this);
  
    return this;
  }

  route (path) {
    this.lazyscene();
    return this._scene.route(path);
  }

  all (path) {
    this.lazyscene();

    const route = this._scene.route(path);
    
    const args = slice.call(arguments, 1);

    for (const type of types) {
      route[type].apply(route, args);
    }

    return this;
  }

  /**
   * Dispatch a req, res pair into the application. Starts pipeline processing.
   *
   * If no callback is provided, then default error handlers will respond
   * in the event of an error bubbling through the stack.
   *
   * @private
   */

  handle (usr, msg) {
    const scene = this._scene;

    function done (layerError) {
      if (layerError) {
        throw new Error(layerError);
      }
    }

    if (usr.isWaiting) {
      usr.handle(msg);
    }

    // no routes
    if (!scene) {
      return done();
    }

    scene.handle(usr, msg, done);
  }
}

types.forEach(function(type) {
  LibTelegram.prototype[type] = function(trigger){
    this.lazyscene();

    const route = this._scene.route(trigger);
    route[type].apply(route, slice.call(arguments, 1));

    return this;
  };
});

exports = module.exports = LibTelegram;

/**
 * Expose constructors.
 */
exports.Route = Route;
exports.Scene = Scene;

/**
 * Expose the prototypes.
 */

 exports.message = null;
 exports.user = null;