const { types } = require('../utils');
const { 
  toRawType,
  slice
} = require('../../shared/utils');

const Layer = require('./layer');

class Route {
  constructor (trigger) {
    this.trigger = trigger;
    this.stack = [];

    this.types = {};
  }

  _handles_type (type) {
    if (this.types._all) {
      return true;
    }
  
    const name = type.toLowerCase();
  
    return Boolean(this.types[name]);
  }

  _options () {
    var types = Object.keys(this.types);

    for (const i in types) {
      types[i] = types[i].toUpperCase();
    }

    return types;
  }

  dispatch (usr, msg, done) {
    let idx = 0;

    const stack = this.stack;
    
    if (stack.length === 0) {
      return done();
    }

    const type = msg.type.toLowerCase();

    msg.route = this;

    next();

    function next(err) {
      // signal to exit route
      if (err && err === 'route') {
        return done();
      }

      // signal to exit router
      if (err && err === 'router') {
        return done(err)
      }

      const layer = stack[idx++];
      if (!layer) {
        return done(err);
      }

      if (layer.type && layer.type !== type) {
        return next(err);
      }

      if (err) {
        layer.handle_error(err, usr, msg, next);
      } else {
        layer.handle_request(usr, msg, next);
      }
    }
  }

  all () {
    const handles = slice.call(arguments).flat();

    for (const handle of handles) {	
      if (typeof handle !== 'function') {
        const type = toString.call(handle);

        throw new Error('Route.all() requires a callback function but got a ' + type);
      }
  
      const layer = new Layer('/', handle);
      layer.type = undefined;
  
      this.types._all = true;
      this.stack.push(layer);
    }
  
    return this;
  }
}

types.forEach(function (type) {
  Route.prototype[type] = function () {
    const handles = slice.call(arguments).flat();
    
    for (const handle of handles) {
      if (typeof handle !== 'function') {
        throw new Error('Route.' + method + '() requires a callback function but got a ' + toRawType(handle));
      }

      const layer = new Layer('all', handle);
      layer.type = type;

      this.types[type] = true;
      this.stack.push(layer);
    }

    return this;
  };
});


module.exports = Route;