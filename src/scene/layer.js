const { toRawType, isTrue } = require('../../shared/utils'),
      { matchText } = require('../utils');

class Layer {
  constructor (trigger, fn) {
    this.handle = fn;
    this.name = fn.name || '<anonymous>';
    this.params = undefined;
    this.trigger = trigger;
  }

  handle_error (error, usr, msg, next) {
    const fn = this.handle;

    if (fn.length !== 4) {
      // not a standard error handler
      return next(error);
    }

    try {
      fn(error, usr, msg, next);
    } catch (err) {
      next(err);
    }
  }

  handle_request (usr, msg, next) {
    const fn = this.handle;

    if (fn.length > 3) {
      // not a request error handler
      return next();
    }

    try {
      fn(usr, msg, next);
    } catch (err) {
      console.log(err);
      next(err);
    }
  }

  match ({ text, caption }) {
    const trigger = this.trigger;

    if ((Array.isArray || typeof trigger === 'text') && trigger.length === 0 || isTrue(trigger)) {
      return true;
    }

    return matchText(text || caption, this.trigger);
  }
}

module.exports = Layer;