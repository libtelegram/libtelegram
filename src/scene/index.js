const Route = require('./route'),
      Layer = require('./layer');

const { slice, toRawType } = require('../../shared/utils'),
      { types } = require('../utils');

class Scene {
	constructor () {
		this.stack = [];
		this.enterHandlers = [];
		this.leaveHandlers = [];
	}

	get mountpath () {
		if (!this.parent) {
			return '/';
		}

		return this.parent.mountpath + this.path + '/'; 
	}

	enter (handler) {
		if (typeof handler !== 'function') {
			throw new Error('Handler should be a function');
		}

		this.enterHandlers.push(handler);
	}

	leave (handler) {
		if (typeof handler !== 'function') {
			throw new Error('Handler should be a function');
		}

		this.leaveHandlers.push(handler);
	}

	executeHandlers (handlers, usr) {
		let match = false, 
				path = usr.path,
		    idx = 0;

		const stack = this.stack;

		while (!match && idx < stack.length) {
			const scene = stack[idx++];

			// ignore layers
			if (!scene.handle || !scene.use) {
				continue;
			}

			if (scene.mountpath === path) {
				for (const handler of scene[handlers]) {
					handler(usr);
				}

				match = true;
			} else {
				match = scene.executeHandlers(handlers, usr);
			}
		}

		return match;
	}

	route (trigger) {
		const route = new Route(trigger);
		route.scene = this;

		const layer = new Layer(trigger, route.dispatch.bind(route));
		layer.route = route;

		this.stack.push(layer);
		return route;
	}

	handle (usr, msg, done) {
		const self = this;

		let idx = 0;

		// routes and middlewares
		const stack = self.stack;

		// setup next layer	
		msg.next = next;

		next();

		function next (err) {
			let layerError;

			// no more matching laters
			if (idx >= stack.length) {
				return;
			}

			let layer,
			    match,
					route;

			while (!match && idx < stack.length) {
				layer = stack[idx++];

				if (layer.handle && layer.use) {
					return layer.handle(usr, msg, next);
				}

				match = matchLayer(layer, msg);
				route = layer.route;

				if (typeof match !== 'boolean') {
					// hold on to layerError
					layerError = layerError || match;
				}

				if (layerError) {
					// routes do not match with a pending error
					match = false;
					continue;
				}

				if (match !== true) {
					continue;
				}
				
				if (!route) {
					// process non-route handlers normally
					continue;
				}

				const type = msg.type;
				
				const has_type = route._handles_type(type);

				if (!has_type) {
					match = false;
					continue;
				}
			}

			if (!match) {
				return done(layerError);
			}

			if (route) {
				msg.route = route;

				if (usr.path !== route.scene.mountpath) {
					return done();
				}
			}
			
			if (layerError) {
				layer.handle_error(layerError, usr, msg, next);

				return done(layerError);
			} else {
				layer.handle_request(usr, msg, next);

				return done();
			}
		}
	}

	use (fn) {
		let offset = 0,
		    path;

		// default path to '/'
		// disambiguate scene.use([fn])
		if (typeof fn !== 'function') {
			let arg = fn;

			while (Array.isArray(arg) && arg.length !== 0) {
				arg = arg[0];
			}

			// first arg is the path
			if (typeof arg !== 'function') {
				offset = 1;
				path = arg;
			}
		}

		const callbacks = slice.call(arguments, offset).flat();

		if (callbacks.length === 0) {
			const err = new Error('Scene.use() requires a middleware function');

			throw err;
		}

		for (var i = 0; i < callbacks.length; i++) {
			var fn = callbacks[i];

			if (fn.handle) {
				fn.path = path || '/';
				fn.parent = this;
				return this.stack.push(fn);
			}

			if (typeof fn !== 'function') {
				const err = new Error('Scene.use() requires a middleware function but got a ' + toRawType(fn));

				throw err;
			}

			// add the middleware
			var layer = new Layer(path || [], fn);

			layer.route = undefined;

			this.stack.push(layer);
		}

		return this;
	}
}

/**
 * Match trigger to a layer.
 *
 * @param {Layer} layer
 * @param {string} trigger
 * @private
 */

function matchLayer(layer, trigger) {
  try {
    return layer.match(trigger);
  } catch (err) {
    return err;
  }
}

types.concat('all').forEach(function(type){
  Scene.prototype[type] = function(trigger){
    const route = this.route(trigger)
    route[type].apply(route, slice.call(arguments, 1));
    return this;
  };
});

module.exports = Scene;