const { toRawType } = require('../shared/utils');

exports.types = [
  'text',
  'command',
  'edited',
  'photo',
  'inline',
  'callback',
  'poll',
  'shiping'
];

/**
 * Match type with message type
 * @param {String | Array} type
 * @param {String} msgType
 * @return {Boolean}
 */
exports.matchType = function (type, msgType) {
  if (Array.isArray(type)) {
    const types = type;

    for (type of types) {
      if (matchType(type, msgType)) {
        return true;
      }
    }
  }

  if (typeof type === 'string') {
    return type === msgType;
  }

  return false;
}

/**
 * Match text and trigger
 * @param {String} text
 * @param {String | Function | RegExp | Array[any]} trigger
 * @return {Boolean}
 */
exports.matchText = function (text, trigger) {
  if (typeof trigger === 'string') {
    return text === trigger;
  }

  if (typeof trigger === 'function') {
    return trigger(text);
  }

  if (toRawType(trigger) === 'RegExp') {
    return trigger.test(text);
  }

  if (Array.isArray(trigger)) {
    const triggers = trigger;

    for (trigger of triggers) {
      if (matchText(text, trigger)) {
        return true;
      }
    }
  }

  return `LibTelegram doesn't support ${toRawType(trigger)} triggers`;
}

