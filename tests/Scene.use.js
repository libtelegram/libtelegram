const LibTelegram = require('..'),
      { Scene } = LibTelegram;

describe('Scene.use', function(){
  it('Scene.use() requires a middleware function', function(done){
    const bot = new Scene();

    try {
      bot.use('/', [/*no functions*/])
    } catch(err) {
      done();
    }
  });
});

