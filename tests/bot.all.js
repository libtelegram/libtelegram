const LibTelegram = require('..');
const e2e = require('../../e2e');

const assert = require('assert');

describe('bot.all', function(){
  it('should response "Hello!" to all requests', function(done){
    const bot = new LibTelegram();

    bot.all('hello', function (usr) {
      usr.reply('Hello!');
    });

    function assertMessage (msg) {
      return assert.strictEqual(msg.text, 'Hello!');
    }

    e2e(bot)()
      .send('hello')
      .wait(assertMessage)
      .photo([1,2,3], 'hello')
      .wait(assertMessage)
      .then(() => done())
      .catch(done);
  });
});

