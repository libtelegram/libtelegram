const LibTelegram = require('..');

const e2e = require('../../e2e');

const assert = require('assert');

describe('bot.handle', function(){
  it('bot should wait for user message', function(done) {
    const bot = new LibTelegram();

    bot.text('/start', async usr => {
      const msg = await usr.wait('text');

      assert.strictEqual(msg.text, 'hello');
      done();
    });

    e2e(bot)()
      .send('/start')
      .wait()
      .send('hello')
      .then()
      .catch(done);
  });

  it('no routes', function() {
    const bot = new LibTelegram();

    bot.handle({/*empty user object*/});
  });

  it('should throw error with notice', function(done) {
    try {
      const bot = new LibTelegram();

      bot.text({}, function() {});

      bot.handle({/*empty user object*/}
                ,{text: '/start'});
    } catch (err) {
      done();
    }
  })
});

