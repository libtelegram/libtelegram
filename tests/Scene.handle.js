const LibTelegram = require('..');

const e2e = require('../../e2e');

describe('Scene.handle', function () {
  it('Scene.handle: shouldn\'nt replied to the messsage', function(){
    const bot = new LibTelegram();

    bot.route('hello')
      .photo(function () {
        done('Shouldn\'t have replied to the message');
      });

    e2e(bot)()
      .send('hello')
      .wait(done)
      .then()
      .catch(() => done());
  });
});