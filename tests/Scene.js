const LibTelegram = require('..'),
      { Scene } = LibTelegram;

const e2e = require('../../e2e');
const { assert } = require('chai');

describe('Scene', function () {
  it('Scene.use: requires a middleware function', function(done){
    const bot = new Scene();
  
    try {
      bot.use('/', 123)
    } catch(err) {
      done();
    }
  });

  it('Scene.enter: handler should be a function', function(done) {
    try {
      const bot = new Scene();

      bot.enter('function');
    } catch (err) {
      done();
    }
  });

  it('Scene.leave: handler should be a function', function(done) {
    try {
      const bot = new Scene();

      bot.leave('function');
    } catch (err) {
      done();
    }
  });

  it('Scene.leave & Scene.enter', function(done) {
    const bot = new LibTelegram();

    bot.text('/enter', function (usr) {
      usr.scene('blog');
    });

    const blog = new Scene();

    blog.text('/leave', function (usr) {
      usr.scene('../');
    });

    blog.enter(function (usr) {
      usr.reply('enter');
    });

    blog.leave(function (usr) {
      usr.reply('leave');
    });

    bot.use('/blog', blog);

    e2e(bot)()
      .send('/enter')
      .wait(function (msg) {
        assert.strictEqual(msg.text, 'enter');
      })
      .send('/leave')
      .wait(function (msg) {
        assert.strictEqual(msg.text, 'leave');
      })
      .then(done)
      .catch(done);
  });
});