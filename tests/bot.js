const LibTelegram = require('../index'),
      { Scene } = LibTelegram;

const assert = require('assert');

require('dotenv').config();

describe('bot', function () {
  it('version', function () {
    assert.strictEqual(LibTelegram.version, '0.0.1');
  });
});

describe('bot.mountpath', function(){
  it('should return the mounted path', function(){
    const bot = new LibTelegram(),
          admin = new Scene(),
          blog = new Scene();

    bot.use('blog', blog);
    blog.use('admin', admin);
    
    assert.strictEqual(admin.mountpath, '/blog/admin/');
    assert.strictEqual(blog.mountpath, '/blog/');
    assert.strictEqual(bot.mountpath, '/');
  });
});

describe('bot polling', function () {
  const bot = new LibTelegram(process.env.TOKEN);

  it('should start polling', function (done) {
    bot.start(done);
  });

  it('should stop polling', function (done) {
    bot.stop(done);
  });
});