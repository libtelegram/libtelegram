const LibTelegram = require('..'),
      { Scene } = LibTelegram;

const e2e = require('../../e2e');

const assert = require('assert');

describe('bot.use', function(){
  it('user name should be "bot"', function(done){
    const bot = new LibTelegram();

    bot.use(function (usr, msg, next) {
      usr.name = 'bot';

      next();
    });

    bot.text('hello', function (usr) {
      if (usr.name === 'bot') {
        return done();
      }

      return done('user name isn\'t "bot"');
    });

    e2e(bot)()
      .send('hello')
      .then()
      .catch(done);
  });
  
  it('should use only first element of array as path', function (done) {
    const bot = new LibTelegram();

    bot.text('/blog', function (usr) {
      usr.scene('blog');
    });

    bot.text('/admin', function (usr) {
      usr.scene('admin');
    });

    const blog = new Scene();

    blog.enter(function (usr) {
      usr.reply('hello');
    });

    bot.use(['/blog', '/admin'], blog);

    e2e(bot)()
      .send('/blog')
      .wait(msg => assert.strictEqual(msg.text, 'hello'))
      .then(function () {
        e2e(bot)()
          .send('/admin')
          .wait(() => done('shouldn\'t have replied to a "/admin"'))
          .then(done)
          .catch(err => done());
      })
      .catch(done);
  });

  it('bot.use() requires a middleware function', function(done){
    const bot = new LibTelegram();

    try {
      bot.use('/', [/*no functions*/])
    } catch(err) {
      done();
    }
  });
});

