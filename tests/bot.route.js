const LibTelegram = require('..');

const e2e = require('../../e2e');

const assert = require('assert');

describe('bot.route', function(){
  it('bot should say hello', function(done) {
    const bot = new LibTelegram();

    bot.route('hello')
      .text(usr => usr.reply('Hello!'));

    function assertMessage (msg) {
      return assert.strictEqual(msg.text, 'Hello!');
    }

    e2e(bot)()
      .send('hello')
      .wait(assertMessage)
      .then(() => done())
      .catch(done);
  });
});

