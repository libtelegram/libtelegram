/*!
 * LibTelegram - Progressive Telegram Bot Framework for NodeJS
 * 
 * Made with <3 by @martiliones
 */

'use strict';

const path = require('path');

const LibTelegram = require('./src/libtelegram');

LibTelegram.version = require(path.resolve(
  __dirname,
  'package.json'
)).version;

module.exports = LibTelegram;