exports.hasOwn = function (obj, key) {
  return obj.hasOwnProperty(key);
}

exports.isTrue = function (val) {
  return val === true;
}

exports.isFalse = function (val) {
  return val === false;
}

exports.isObject = function (val) {
  return val !== null && typeof val === 'object';
}

/**
 * Get the raw type string of a value, e.g., [object Object].
 */

const _toString = Object.prototype.toString;

exports.toRawType = function (value) {
  return _toString.call(value).slice(8, -1)
}

exports.isFunc = function (val) {
  return typeof val === 'function';
}

exports.isRegExp = function (val) {
  return _toString.call(val) === '[object RegExp]';
}

/**
 * Strict object type check. Only returns true
 * for plain JavaScript objects.
 */
exports.isPlainObject = function (val) {
  return _toString.call(val) === '[object Object]';
}

exports.isUndef = function (val) {
  return val === undefined || val === null;
}

exports.isDef = function (val) {
  return val !== undefined && val !== null;
}

exports.def = function (obj, key, extra) {
  Object.defineProperty(obj, key, extra);
}

exports.isPromise = function (val) {
  return (
    isDef(val) &&
    typeof val.then === 'function' &&
    typeof val.catch === 'function'
  )
}

exports.isPrimitive = function (val) {
  return (
    typeof val === 'string' ||
    typeof val === 'number' ||
    typeof val === 'symbol' ||
    typeof val === 'boolean'
  );
}

exports.mergeOptions = function (options, defaultOptions = {}) {
  if (options && defaultOptions) {
    Object.assign(
      options, 
      defaultOptions
    );
  }

  return options;
}

exports.slice = Array.prototype.slice;