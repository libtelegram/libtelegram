const LibTelegram= require('.'),
      { Scene } = LibTelegram;

require('dotenv').config();

const bot = new LibTelegram(process.env.TOKEN);

bot.text('/blog', function (usr) {
  usr.scene('blog');
});

bot.text('/admin', function (usr) {
  usr.scene('admin');
});

const blog = new Scene();

blog.enter(function (usr) {
  usr.reply('hello');
});

bot.use(['/blog', '/admin'], blog);

bot.startPolling();