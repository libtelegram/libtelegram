// user utils

exports.reply = function reply (text, extra) {
  return function (usr) {
    usr.reply(text, extra);
  }
}